/// <reference types="node" />
import {Framework, Utils} from "floose";
import Server = Framework.Server;

/**
 * Components
 */
export interface Packet {
    /**
     * Packet event name
     */
    event: string;

    /**
     * Any data argument included in packet, callback function is not included
     */
    data: any[];
}
export interface Socket {
    /**
     * The ID for this socket
     */
    readonly id: string;

    /**
     * The list of groups that this Socket is currently in
     */
    readonly groups: string[];

    /**
     * The headers passed along with the request.
     */
    readonly headers: {[key: string]: string};

    /**
     * Any query string parameters in the request url
     */
    readonly query: {[key: string]: string} | string[] | string;

    /**
     * The timestamp for when this was issued
     */
    readonly issued: number;

    /**
     * dynamic data stored during processing
     */
    artifacts: {[key: string]: any};
}

/**
 * Integrations
 */
export interface Listener {
    readonly event: string;
    execute: (socket: Socket, data: any[]) => Promise<any | void>;
}
export declare enum MiddlewareEvent {
    onConnect = "onConnect",
    onPacket = "onPacket",
    onDisconnecting = "onDisconnecting",
    onDisconnect = "onDisconnect"
}
export interface MiddlewareOnConnect {
    readonly event: MiddlewareEvent.onConnect;
    execute: (socket: Socket) => Promise<Socket | void>;
}
export interface MiddlewareOnPacket {
    readonly event: MiddlewareEvent.onPacket;
    execute: (socket: Socket, packet: Packet) => Promise<Packet | void>;
}
export interface MiddlewareOnDisconnecting {
    readonly event: MiddlewareEvent.onDisconnecting;
    execute: (socket: Socket, reason: string) => Promise<void>;
}
export interface MiddlewareOnDisconnect {
    readonly event: MiddlewareEvent.onDisconnect;
    execute: (socket: Socket, reason: string) => Promise<void>;
}
export declare type Middleware = MiddlewareOnConnect | MiddlewareOnPacket | MiddlewareOnDisconnecting | MiddlewareOnDisconnect;

/**
 * Server
 */
export interface SocketIOServerConfig {
    /**
     * An array of fully qualified domain names that can access to server according to Cross-Origin Resource Sharing.
     * The array can contain any combination of fully qualified origins along with origin strings containing a
     * wildcard '*' character, or a single '*' origin string.
     */
    origins: '*' | string[];
    /**
     * The TCP port the server will listen to
     */
    port: number;
}

export declare class SocketIOServer extends Server {
    readonly configurationValidationSchema: Utils.Validation.Schema;
    readonly running: boolean;
    init(config: SocketIOServerConfig): Promise<void>;
    start(): Promise<void>;
    stop(): Promise<void>;

    /**
     *
     * @param middleware
     */
    addMiddleware(middleware: Middleware): string;

    /**
     *
     * @param identifier
     */
    removeMiddleware(identifier: string): void;

    /**
     *
     * @param listener
     */
    addListener(listener: Listener): string;

    /**
     *
     * @param identifier
     */
    removeListener(identifier: string): void;

    /**
     *
     * @param packet
     * @param recipients
     * @param callback
     */
    dispatch(packet: Packet, recipients?: {
        socket?: string | string[],
        excludeSocket?: string,
        group?: string | string[]
    }, callback?: (data: any[]) => void): void;
}